#!/usr/bin/ruby

require 'watir-webdriver'

class Fetcher
    def self.remove_tooltip browser
        tooltip = browser.div(:id => 'ui-tooltip-0')
        tooltip.wait_until_present
        puts 'tooltip appeared'

        tooltip.a(:class => 'ui-tooltip-close').click
        puts 'tooltip closed'
    end

    def initialize(*args)
        @loader, @wrapper = args
    end

    def process_li li
        filename = li.attribute_value(:id)

        if File.exists? filename then
            puts "#{filename} already exists"
            return
        end

        li.a(:xpath => './*').click
        @loader.wait_while_present

        content = @wrapper.elements(:xpath => './div/*')

        File.open(filename, 'w') do |f|
            content.each do |item|
                f.puts item.html
            end
        end

        puts "Dumped #{filename}"
    end
end


browser = Watir::Browser.start 'http://catalog.flatworldknowledge.com/bookhub/reader/21'

Fetcher.remove_tooltip browser

fetcher = Fetcher.new(
    browser.div(:id => 'book-main').div(:id => 'content-loader'),
    browser.div(:id => 'book-main').div(:id => 'book-content')
)

browser.ul(:id => 'toc').lis(:xpath => './*').each do |li|
    fetcher.process_li li

    li.lis(:xpath => './ul/*').each do |sub|
        fetcher.process_li sub
    end
end

browser.close

